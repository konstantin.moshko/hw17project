﻿#include <iostream>
class Test
{
private:
    int a, b, c;

public:
    Test(int a1, int b1, int c1) : a(a1), b(b1), c(c1)
    {}
    
    void print()
    {
         std::cout << a << "\n" << b << "\n" << c << "\n";
    }

};

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
    double module;

public:
    Vector() : x(8), y(9), z(15)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
        void show()
    {
        module = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << "\n" << x << " " << y << " " << z << "\nModule = " << module;
    }
};

int main()
{
    Test r{ 5,6,7 };
    r.print();

    Vector v;
    v.show();

    return 0;
}